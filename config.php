<?php
use app\controllers\IndexController;
use app\controllers\TaskController;
use app\controllers\UserController;
use app\interfaces\views\IView;
use lindal\webhelper\interfaces\IRequest;
use lindal\webhelper\interfaces\IResponse;
use lindal\webhelper\Request;
use lindal\webhelper\Response;

$host = 'db';
$db = 'dev';
$user = 'root';
$pass = '123456';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];

return [
    'definitions' => [
        PDO::class => \DI\object()
            ->constructor($dsn, $user, $pass, $opt),
        IView::class => \DI\object(\app\views\View::class),
        IRequest::class => \DI\object(Request::class),
        IResponse::class => \DI\object(Response::class)
    ],
    'routing' => [
        [
            'className' => IndexController::class,
            'handler' => 'indexAction',
            'pattern' => '/'
        ],
        [
            'className' => TaskController::class,
            'handler' => 'createAction',
            'pattern' => '/task/create',
            'method' => 'GET'
        ],
        [
            'className' => TaskController::class,
            'handler' => 'createAction',
            'pattern' => '/task/create',
            'method' => 'POST'
        ],
        [
            'className' => TaskController::class,
            'handler' => 'updateAction',
            'pattern' => '/task/update/{id}',
            'method' => 'POST'
        ],
        [
            'className' => TaskController::class,
            'handler' => 'updateAction',
            'pattern' => '/task/update/{id}',
            'method' => 'GET'
        ],
        [
            'className' => TaskController::class,
            'handler' => 'previewAction',
            'pattern' => '/task/preview',
            'method' => 'POST'
        ],
        [
            'className' => UserController::class,
            'handler' => 'loginAction',
            'pattern' => '/user/login',
            'method' => 'POST'
        ],
        [
            'className' => UserController::class,
            'handler' => 'logoutAction',
            'pattern' => '/user/logout',
            'method' => 'GET'
        ]
    ]
];