function sendLoginForm() {
    var form = $('.login-form form');
    $.ajax('/user/login', {
        method: 'POST',
        data: form.serialize(),
        dataType: 'JSON',
        success: function (response) {
            if (response.success) {
                window.location.href = '/';
            }
            else {
                $('.login-form-errors').html(response.errors);
            }
        }
    });
}

function taskPreview() {
    var form = $('#task-form');
    $.ajax('/task/preview', {
        method: 'POST',
        data: form.serialize(),
        success: function (response) {
            $('#task-modal .modal-body').html(response);
            $('#task-modal').modal('show');
        }
    });
}

$(document).ready(function () {
    if ($('#search-modal').hasClass('default-open')) {
        $('#search-modal').modal('show');
    }
});