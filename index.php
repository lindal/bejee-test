<?php
use app\base\App;

require_once 'vendor/autoload.php';

$config = require 'config.php';

App::run($config);
