<?php

namespace app\repositories;

use app\interfaces\repositories\IRepository;
use PHPUnit\Runner\Exception;

class DbRepository implements IRepository
{

    private $_filters = [];
    private $_orders = [];
    private $_limit = 20;
    private $_offset = 0;
    private $_table;

    /**
     * @var \PDO
     */
    protected $_pdo;

    public function __construct(\PDO $pdo, string $table)
    {
        $this->_pdo = $pdo;
        $this->_table = $table;
    }

    /**
     * Load model by ID
     * @param int $id
     * @return array|null
     */
    public function loadById(int $id)
    {
        $this->_filters = [];
        $this->addFilter('id', $id);
        $result = $this->load();
        return $result[0] ?? null;
    }

    /**
     * Add filter to query
     * @param string $field
     * @param string $value
     * @param string $type
     * @return IRepository
     */
    public function addFilter(string $field, string $value, string $type = 'and'): IRepository
    {
        $this->_filters[] = [
            $field, $value, $type
        ];
        return $this;
    }

    /**
     * Clear filter data
     * @return IRepository
     */
    public function clearFilter(): IRepository
    {
        $this->_filters = [];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dir
     * @return IRepository
     */
    public function addOrder(string $field, string $dir = 'ASC'): IRepository
    {
        if (!in_array($dir, ['ASC', 'DESC'])) {
            throw new Exception('Invalid order direction');
        }
        $this->_orders[] = [$field, $dir];
        return $this;
    }

    /**
     * Clear order data
     * @return IRepository
     */
    public function clearOrder(): IRepository
    {
        $this->_orders = [];
        return $this;
    }

    /**
     * Set order to query
     * [field => direction, ...]
     * @param array $order
     * @return IRepository
     */
    public function setOrder(array $order): IRepository
    {
        $this->_orders = $order;
        return $this;
    }

    /**
     * Return order
     * @return array
     */
    public function getOrder(): array
    {
        return $this->_orders;
    }

    /**
     * Get limit
     * @return int
     */
    public function getLimit(): int
    {
        return $this->_limit;
    }

    /**
     * Set limit
     * @param int $limit
     * @return IRepository
     */
    public function setLimit(int $limit): IRepository
    {
        $this->_limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->_offset;
    }

    /**
     * Set offset
     * @param int $page
     * @return IRepository
     */
    public function setOffset(int $page): IRepository
    {
        $this->_offset = $page;
        return $this;
    }

    /**
     * Return result
     * @return array
     */
    public function load(): array
    {
        $where = $this->prepareWhere() ? ' WHERE ' . $this->prepareWhere() : '';
        $limit = $this->prepareLimit() ? ' LIMIT ' . $this->prepareLimit() : '';
        $order = $this->prepareOrder() ? ' ORDER BY ' . $this->prepareOrder() : '';
        $sql = 'SELECT * FROM ' . $this->_table . $where . $order . $limit;
        $stmt = $this->_pdo
            ->prepare($sql);
        $stmt->execute($this->prepareParams());
        return $stmt->fetchAll();
    }

    /**
     * Return total count
     * @return int
     */
    public function getCount(): int
    {
        $where = $this->prepareWhere() ? ' WHERE ' . $this->prepareWhere() : '';
        $sql = 'SELECT count(*) FROM ' . $this->_table . $where;
        $stmt = $this->_pdo
            ->prepare($sql);
        $stmt->execute($this->prepareParams());
        return $stmt->fetchColumn();
    }

    private function prepareOrder()
    {
        $orders = [];
        foreach ($this->_orders as $order) {
            $orders[] = $order[0] . ' ' . $order[1];
        }
        return implode(', ', $orders);
    }

    private function prepareLimit()
    {
        return $this->_offset . ',' . $this->_limit;
    }

    private function prepareWhere()
    {
        $where = '';
        foreach ($this->_filters as $key => $filter) {
            if ($key == 0) {
                $where .= is_int($filter[1]) ? $this->prepareCondition($filter[0]) : $this->prepareLikeCondition($filter[0]);
            } else {
                $where .= ' ' . $filter[2] . ' ' . $this->prepareCondition($filter[0]);
            }
        }
        return $where;
    }

    private function prepareCondition(string $field)
    {
        return $field . ' = :' . $field;
    }

    private function prepareLikeCondition(string $field)
    {
        return $field . ' LIKE :' . $field;
    }

    private function prepareParams()
    {
        $params = [];
        foreach ($this->_filters as $filter) {
            $key = ':' . $filter[0];
            $params[$key] = $filter[1];
        }
        return $params;
    }

    public function update(int $id, array $data): bool
    {
        $bind = $this->prepareBindFromData($data);
        $params = $this->prepareParamsFromData($data);
        $params['id'] = $id;
        return (bool)$this->_pdo
            ->prepare('UPDATE ' . $this->_table . ' SET ' . $bind . ' WHERE id = :id')
            ->execute($params);
    }

    /**
     * Insert new row to DB and return inserted ID
     * @param array $data
     * @return int inserted id
     */
    public function insert(array $data): int
    {
        $fields = array_keys($data);
        foreach ($data as $field => $value) {
            $values[] = ':' . $field;
        }
        $params = $this->prepareParamsFromData($data);
        $fields = '(' . implode(', ', $fields) . ')';
        $values = '(' . implode(', ', $values) . ')';
        $this->_pdo
            ->prepare('INSERT INTO ' . $this->_table . '  ' . $fields . ' VALUES ' . $values)
            ->execute($params);
        return $this->_pdo->lastInsertId();
    }

    private function prepareParamsFromData(array $data): array
    {
        $params = [];
        foreach ($data as $field => $value) {
            $paramName = ':' . $field;
            $params[$paramName] = $value;
        }
        return $params;
    }

    /**
     * @param array $data
     * @return string
     */
    private function prepareBindFromData(array $data): string
    {
        $bind = [];
        foreach ($data as $field => $value) {
            if ($field == 'id') {
                continue;
            }
            $bind[] = $this->prepareCondition($field);
        }
        return implode(', ', $bind);
    }

    /**
     * Delete row by ID
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->_pdo
            ->prepare('DELETE FROM ' . $this->_table . ' WHERE id = :id')
            ->execute(['id' => $id]);
    }

}