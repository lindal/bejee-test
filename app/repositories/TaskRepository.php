<?php
/**
 * Created by PhpStorm.
 * User: lindal
 * Date: 26.08.17
 * Time: 23:31
 */

namespace app\repositories;


class TaskRepository extends DbRepository
{

    public function __construct(\PDO $pdo, $table = 'tasks')
    {
        parent::__construct($pdo, $table);
    }

}