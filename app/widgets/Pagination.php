<?php

namespace app\widgets;

use app\views\View;
use lindal\webhelper\Request;

class Pagination
{

    public $request;

    public $view;

    public $itemsCount = 4;

    public $pageCount = 1;

    /**
     * GET param name for page
     * @var string
     */
    public $pageParam = 'p';

    public $template = 'widgets/pagination';

    public function __construct(Request $request, View $view)
    {
        $this->request = $request;
        $this->view = $view;
    }

    /**
     * Return current page
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->request->get($this->pageParam, 1);
    }

    public function render()
    {
        return $this->view->render($this->template, [
            'widget' => $this
        ]);
    }

    public function getItems(): array
    {
        $current = $this->getCurrentPage();
        $items = [];
        for ($i = 1; $i <= $this->pageCount; $i++) {
            $items[] = [
                'label' => $i,
                'url' => $this->createUrl($i),
                'current' => $current == $i
            ];
        }
        return $items;
    }

    private function createUrl(int $page): string
    {
        $params = $this->request->get();
        $params[$this->pageParam] = $page;
        $queryString = [];
        foreach ((array)$params as $name => $value) {
            $queryString[] = $name . '=' . $value;
        }
        return $this->removeQueryString($this->request->getUri()) . '?' . implode('&', $queryString);
    }

    private function removeQueryString(string $uri): string
    {
        $pos = strpos($uri, '?');
        if ($pos !== false) {
            $length = strpos($uri, '?') ? strpos($uri, '?') : null;
            return substr($uri, 0, $length);
        }
        return $uri;
    }
}