<?php

namespace app\errors;

class Unauthorized extends \Exception
{

    protected $code = 401;
    protected $message = 'Unauthorized';

}