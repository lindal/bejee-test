<?php

namespace app\errors;

class NotFound extends \Exception
{

    protected $code = 404;
    protected $message = 'Page not found';

}