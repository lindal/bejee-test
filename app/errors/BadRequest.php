<?php

namespace app\errors;


class BadRequest extends \Exception
{

    protected $code = 400;
    protected $message = 'Bad request';

}