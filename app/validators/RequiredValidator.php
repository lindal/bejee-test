<?php

namespace app\validators;

use app\interfaces\models\IForm;
use app\interfaces\validators\IValidator;

class RequiredValidator implements IValidator
{

    /**
     * @param IForm $model
     * @param string $attribute
     * @param array $params
     * @return bool
     */
    public function run(IForm $model, string $attribute, array $params = []): bool
    {
        if (!$model->{$attribute}) {
            $model->addError($attribute, $attribute . ' is required');
            return false;
        }
        return true;
    }

}