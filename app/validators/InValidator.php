<?php

namespace app\validators;


use app\interfaces\models\IForm;
use app\interfaces\validators\IValidator;

class InValidator implements IValidator
{

    /**
     * @param IForm $model
     * @param string $attribute
     * @param array $values
     * @return bool
     * @throws \Exception
     */
    public function run(IForm $model, string $attribute, array $values = []): bool
    {
        if (!$values) {
            throw new \Exception('Param must be set');
        }
        if (!empty($model->{$attribute}) && !in_array($model->{$attribute}, $values)) {
            $model->addError($attribute, $attribute . ' has not allowed value');
            return false;
        }
        return true;
    }
}