<?php

namespace app\validators;


use app\interfaces\models\IForm;
use app\interfaces\validators\IValidator;

class IsIntegerValidator implements IValidator
{

    /**
     * @param IForm $model
     * @param string $attribute
     * @param array $params
     * @return bool
     */
    public function run(IForm $model, string $attribute, array $params = []): bool
    {
        if (!empty($model->{$attribute}) && !is_integer((int)$model->{$attribute})) {
            $model->addError($attribute, $attribute . ' must be an integer');
            return false;
        }
        return true;
    }
}