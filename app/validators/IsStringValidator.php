<?php

namespace app\validators;


use app\interfaces\models\IForm;
use app\interfaces\validators\IValidator;

class IsStringValidator implements IValidator
{

    /**
     * @param IForm $model
     * @param string $attribute
     * @param array $params
     * @return bool
     */
    public function run(IForm $model, string $attribute, array $params = []): bool
    {
        if (!empty($model->{$attribute}) && !is_string($model->{$attribute})) {
            $model->addError($attribute, $attribute . ' must be a string');
            return false;
        }
        return true;
    }
}