<?php

namespace app\models;


use app\interfaces\models\IModel;
use app\interfaces\repositories\IRepository;
use app\repositories\TaskRepository;

class Task implements IModel
{

    private $_id;
    /**
     * @var IRepository
     */
    private $_repository;

    const STATUS_TODO = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DONE = 3;

    public static $statusNames = [
        self::STATUS_TODO => 'todo',
        self::STATUS_IN_PROGRESS => 'in progress',
        self::STATUS_DONE => 'done',
    ];

    private $_data = [];

    public function __construct(TaskRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getDataByName(string $name)
    {
        return $this->_data[$name] ?? null;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return IModel
     */
    public function setDataByName(string $name, $value): IModel
    {
        $this->_data[$name] = $value;
        return $this;
    }

    /**
     * Set data into model
     * @param array $data
     * @return IModel
     */
    public function setData(array $data): IModel
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * Return all data
     * @return array
     */
    public function getData(): array
    {
        return $this->_data;
    }

    /**
     * Return model ID
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $id
     * @return IModel
     */
    public function setId($id): IModel
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @param $id
     * @return IModel
     */
    public function load($id): IModel
    {
        $data = $this->_repository->loadById($id);
        if ($data) {
            $this->setId($data['id']);
            unset($data['id']);
            $this->setData($data);
        }
        return $this;
    }

    /**
     * Save model into repository
     * @return null
     */
    public function save()
    {
        if (!$this->getId()) {
            return $this->_repository->insert($this->getData());
        }
        return $this->_repository->update($this->getId(), $this->getData());
    }

    public function setRepository(IRepository $repository): IModel
    {
        $this->_repository = $repository;
        return $this;
    }

    /**
     * Return model`s repository or null if not set
     * @return IRepository|null
     */
    public function getRepository()
    {
        return $this->_repository;
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return self::$statusNames[$this->getDataByName('status')] ?? '';
    }
}