<?php

namespace app\models\forms;


use app\base\Session;
use app\validators\RequiredValidator;
use lindal\webhelper\interfaces\IRequest;

class Login extends AbstractForm
{

    public $username;
    public $password;

    /**
     * @var Session
     */
    private $_session;

    const VALID_USERNAME = 'admin';
    const VALID_PASSWORD = '123';


    public function __construct(Session $session, IRequest $request)
    {
        $this->_session = $session;
        $this->_validators = [
            'username' => [
                [RequiredValidator::class]
            ],
            'password' => [
                [RequiredValidator::class]
            ]
        ];
        $this->username = $request->post('username');
        $this->password = $request->post('password');
    }

    /**
     * Validate data
     * @return bool
     */
    public function validate(): bool
    {
        $valid = parent::validate();
        if ($this->username != self::VALID_USERNAME || $this->password != self::VALID_PASSWORD) {
            $this->addError('username', 'Username or password is invalid.');
            return false;
        }
        return $valid;
    }

    /**
     * @return void
     */
    public function login()
    {
        $this->_session->setIsAdmin(true);
    }
}