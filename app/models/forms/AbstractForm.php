<?php

namespace app\models\forms;


use app\interfaces\models\IForm;
use DI\ContainerBuilder;

abstract class AbstractForm implements IForm
{

    private $_errors = [];

    /**
     * [
     *  $attribute => [
     *      ['validator class name', $params = []],
     *      ....
     *  ]
     * ]
     * @var array
     */
    protected $_validators = [];

    /**
     * Add error to errors list
     * @param string $attribute
     * @param string $message
     * @return IForm
     */
    public function addError(string $attribute, string $message): IForm
    {
        $this->_errors[] = [
            'attribute' => $attribute,
            'message' => $message
        ];
        return $this;
    }

    /**
     * Return errors list
     * @return array
     */
    public function getErrors(): array
    {
        return $this->_errors;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        $valid = true;
        $container = ContainerBuilder::buildDevContainer();
        foreach ($this->_validators as $attribute => $data) {
            foreach ((array)$data as $validatorData) {
                $validatorObject = $container->get($validatorData[0]);
                $valid &= $validatorObject->run($this, $attribute, $validatorData[1] ?? []);
            }
        }
        return $valid;
    }
}