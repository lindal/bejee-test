<?php

namespace app\models\forms;


use app\models\ImageUploader;
use app\models\Task;
use app\validators\InValidator;
use app\validators\IsIntegerValidator;
use app\validators\IsStringValidator;
use app\validators\RequiredValidator;
use lindal\webhelper\Request;

class TaskForm extends AbstractForm
{

    public $author;
    public $email;
    public $status;
    public $text;

    /**
     * @var Task
     */
    private $_model;
    private $_request;
    private $_uploader;
    const UPLOADED_FILE_PARAM_NAME = 'uploaded_file';

    public function __construct(Request $request, ImageUploader $uploader)
    {
        $this->_request = $request;
        $this->_uploader = $uploader;

        $this->author = $request->post('author');
        $this->text = $request->post('text');
        $this->email = $request->post('email');
        $this->status = $request->post('status', Task::STATUS_TODO);

        $this->_validators = [
            'author' => [
                [RequiredValidator::class],
                [IsStringValidator::class]
            ],
            'email' => [
                [RequiredValidator::class],
                [IsStringValidator::class]
            ],
            'status' => [
                [RequiredValidator::class],
                [IsIntegerValidator::class],
                [InValidator::class, array_keys(Task::$statusNames)]
            ],
            'text' => [
                [RequiredValidator::class],
                [IsStringValidator::class]
            ],
        ];
    }

    public function validate(): bool
    {
        $valid = parent::validate();
        $files = $this->_request->getUploadedFiles();
        if (!$this->_model->getId() && !isset($files[self::UPLOADED_FILE_PARAM_NAME])) {
            $this->addError(self::UPLOADED_FILE_PARAM_NAME, 'File is required');
            return false;
        }
        if (isset($files[self::UPLOADED_FILE_PARAM_NAME])) {
            $this->_uploader->setFile($files[self::UPLOADED_FILE_PARAM_NAME]);
            if (!$this->_uploader->validate()) {
                $this->addError(self::UPLOADED_FILE_PARAM_NAME, 'File has wrong type');
                return false;
            }
        }
        return $valid;
    }

    /**
     * @param Task $model
     * @return TaskForm
     */
    public function setTask(Task $model)
    {
        $this->_model = $model;

        $this->author = $this->author ?? $model->getDataByName('author');
        $this->email = $this->email ?? $model->getDataByName('email');
        $this->status = $this->status ?? $model->getDataByName('status');
        $this->text = $this->text ?? $model->getDataByName('text');
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->_model;
    }

    public function save()
    {
        $this->_model->setData([
            'author' => $this->author,
            'email' => $this->email,
            'text' => $this->text,
            'status' => $this->status,
        ]);
        if ($this->_request->getUploadedFiles()) {
            $this->_model->setDataByName('image', $this->saveImage());
        }
        $this->_model->save();
    }

    /**
     * @return string url to saved image
     */
    private function saveImage(): string
    {
        $file = $this->_request->getUploadedFiles();
        return $this->_uploader->setFile($file[self::UPLOADED_FILE_PARAM_NAME])->save();
    }

}