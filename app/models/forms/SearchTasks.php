<?php

namespace app\models\forms;

use app\collections\TaskCollection;
use app\models\Task;
use app\validators\InValidator;
use app\validators\IsIntegerValidator;
use app\validators\IsStringValidator;
use lindal\webhelper\Request;

class SearchTasks extends AbstractForm
{

    public $order;
    public $dir = 'ASC';
    public $author;
    public $email;
    public $status;
    public $page;
    public $perPage = 3;

    public $allowedOrder = [
        '', 'author', 'email', 'status', 'created_at'
    ];

    public $allowedDir = [
        'ASC', 'DESC'
    ];

    /**
     * @var TaskCollection
     */
    private $collection;

    public function __construct(Request $request, TaskCollection $collection)
    {
        $this->collection = $collection;

        $this->order = $request->get('order');
        $this->dir = $request->get('dir');
        $this->author = $request->get('author');
        $this->email = $request->get('email');
        $this->status = $request->get('status');
        $this->page = (int)$request->get('p', 1);

        $this->_validators = [
            'author' => [
                [IsStringValidator::class]
            ],
            'email' => [
                [IsStringValidator::class]
            ],
            'status' => [
                [IsIntegerValidator::class],
                [InValidator::class, array_keys(Task::$statusNames)]
            ],
            'order' => [
                [IsStringValidator::class],
                [InValidator::class, $this->allowedOrder]
            ],
            'dir' => [
                [IsStringValidator::class],
                [InValidator::class, $this->allowedDir]
            ]
        ];
    }

    public function getCollection()
    {
        $this->collection->setSize($this->perPage);
        $this->collection->setCurrentPage($this->page);
        $this->collection->load();
        return $this->collection;
    }

    public function applyFilters()
    {
        if ($this->author) {
            $this->collection->addFilter('author', $this->author);
        }
        if ($this->email) {
            $this->collection->addFilter('email', $this->email);
        }
        if ($this->status) {
            $this->collection->addFilter('status', $this->status);
        }
        if ($this->order) {
            $this->collection->addOrder($this->order, $this->dir);
        }
    }

}