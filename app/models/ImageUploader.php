<?php

namespace app\models;


use app\base\App;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use lindal\webhelper\UploadedFile;

class ImageUploader
{

    /**
     * @var UploadedFile
     */
    private $_file;
    private $_allowedTypes = [
        'image/png',
        'image/jpg',
        'image/jpeg',
        'image/gif'
    ];
    /**
     * @var Imagine
     */
    private $_imagine;
    const PATH = 'images/';
    const WIDTH = 320;
    const HEIGHT = 240;

    public function __construct(Imagine $imagine)
    {
        $this->_imagine = $imagine;
    }

    /**
     * @return string path to saved image
     */
    public function save(): string
    {
        $filename = $this->getFilename();
        $image = $this->_imagine->open($this->_file->getTmp());
        if ($image->getSize()->getHeight() > $image->getSize()->getWidth()) {
            $box = $image->getSize()->heighten(self::HEIGHT);
        } else {
            $box = $image->getSize()->widen(self::WIDTH);
        }
        $image->resize($box)->save(App::getBaseDir() . $filename);
        return '/' . $filename;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        if (!file_exists($this->_file->getTmp()) || !in_array($this->_file->getType(), $this->_allowedTypes)) {
            return false;
        }
        return true;
    }

    /**
     * @param UploadedFile $file
     * @return ImageUploader
     */
    public function setFile(UploadedFile $file): ImageUploader
    {
        $this->_file = $file;
        return $this;
    }

    private function getFilename()
    {
        return self::PATH . uniqid() . $this->_file->getExtension();
    }

}