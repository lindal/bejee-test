<?php

namespace app\controllers;

use app\base\App;
use app\base\Session;
use app\collections\TaskCollection;
use app\interfaces\views\IView;
use app\models\forms\SearchTasks;
use app\widgets\Pagination;
use lindal\webhelper\interfaces\IRequest;
use lindal\webhelper\interfaces\IResponse;

class IndexController
{

    /**
     * Action for main page
     * @param IRequest $request
     * @param IResponse $response
     * @param array $params
     */
    public function indexAction(IRequest $request, IResponse $response, $params = [])
    {
        $di = App::getInstance()->container;
        $session = $di->get(Session::class);
        $form = $di->get(SearchTasks::class);

        if ($request->get('search', null) && $form->validate()) {
            $form->applyFilters();
        }
        
        $collection = $form->getCollection();

        $pagination = $di->get(Pagination::class);
        $pagination->pageCount = ceil($collection->getTotalCount() / $collection->getSize());

        $output = $di->get(IView::class)->render('index', [
            'title' => 'Task list',
            'search' => $form,
            'collection' => $collection,
            'pagination' => $pagination,
            'isAdmin' => $session->isAdmin()
        ]);
        $response->setBody($output)->send();
    }
}