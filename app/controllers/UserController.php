<?php

namespace app\controllers;


use app\base\App;
use app\base\Session;
use app\interfaces\views\IView;
use app\models\forms\Login;
use lindal\webhelper\interfaces\IRequest;
use lindal\webhelper\interfaces\IResponse;

class UserController
{

    public function loginAction(IRequest $request, IResponse $response, array $params = [])
    {
        $di = App::getInstance()->container;
        $return = [
            'success' => true,
            'errors' => ''
        ];
        $model = $di->get(Login::class);
        if ($model->validate()) {
            $model->login();
        } else {
            $return['errors'] = $di->get(IView::class)->render('_formError', ['model' => $model]);
            $return['success'] = false;
        }
        $response
            ->setBody(json_encode($return))
            ->send();
    }

    public function logoutAction(IRequest $request, IResponse $response, array $params = [])
    {
        $session = App::getInstance()->container->get(Session::class);
        $session->setIsAdmin(false);
        $response->redirect('/');
    }

}