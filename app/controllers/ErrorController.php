<?php
/**
 * Created by PhpStorm.
 * User: lindal
 * Date: 28.08.17
 * Time: 13:10
 */

namespace app\controllers;


use app\base\App;
use app\interfaces\views\IView;
use lindal\webhelper\interfaces\IRequest;
use lindal\webhelper\interfaces\IResponse;

class ErrorController
{

    /**
     * @param IRequest $request
     * @param IResponse $response
     * @param \Exception $exception
     */
    public function indexAction(IRequest $request, IResponse $response, \Exception $exception)
    {
        $di = App::getInstance()->container;
        $errorPage = $di->get(IView::class)->render('error', [
            'exception' => $exception
        ]);
        $response->setCode($exception->getCode());
        $response->setBody($errorPage);
        $response->send();
    }

}