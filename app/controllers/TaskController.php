<?php

namespace app\controllers;


use app\base\App;
use app\base\Session;
use app\errors\BadRequest;
use app\errors\NotFound;
use app\errors\Unauthorized;
use app\interfaces\views\IView;
use app\models\forms\TaskForm;
use app\models\Task;
use lindal\webhelper\interfaces\IRequest;
use lindal\webhelper\interfaces\IResponse;

class TaskController
{

    /**
     * Create new task
     * @param IRequest $request
     * @param IResponse $response
     * @param array $params
     */
    public function createAction(IRequest $request, IResponse $response, $params = [])
    {
        $di = App::getInstance()->container;
        $session = $di->get(Session::class);
        $model = $di->get(TaskForm::class)
            ->setTask($di->get(Task::class));

        if ($request->isPost() && $model->validate()) {
            $model->save();
            $response->redirect('/');
            return;
        }
        $result = $di->get(IView::class)->render('taskForm', [
            'title' => 'Create new task',
            'model' => $model,
            'isAdmin' => $session->isAdmin()
        ]);
        $response->setBody($result)->send();
    }

    /**
     * @param IRequest $request
     * @param IResponse $response
     * @param array $params
     * @throws BadRequest
     * @throws NotFound
     * @throws Unauthorized
     */
    public function updateAction(IRequest $request, IResponse $response, $params = [])
    {
        $di = App::getInstance()->container;

        $session = $di->get(Session::class);

        if (!$session->isAdmin()) {
            throw new Unauthorized();
        }

        if (!isset($params['id']) || !is_numeric($params['id'])) {
            throw new BadRequest();
        }

        $task = $di->get(Task::class)->load($params['id']);

        if (!$task->getId()) {
            throw new NotFound('Task not found');
        }
        $model = $di->get(TaskForm::class);
        $model->setTask($task);

        if ($request->isPost() && $model->validate()) {
            $model->save();
            $response->redirect('/');
            return;
        }

        $result = $di->get(IView::class)->render('taskForm', [
            'title' => 'Update the task',
            'model' => $model,
            'isAdmin' => $session->isAdmin()
        ]);
        $response->setBody($result)->send();
    }

    /**
     * @param IRequest $request
     * @param IResponse $response
     * @param array $params
     * @throws BadRequest
     * @throws NotFound
     */
    public function previewAction(IRequest $request, IResponse $response, $params = [])
    {
        $di = App::getInstance()->container;

        $model = $di->get(Task::class)->setData($request->post());
        $model->setDataByName('created_at', date('Y-m-d H:i:s'));
        if (!$model->getDataByName('status')) {
            $model->setDataByName('status', Task::STATUS_TODO);
        }

        $html = $di->get(IView::class)->render('_task', [
            'model' => $model,
            'isAdmin' => false
        ]);
        $response->setBody($html)->send();
    }

}