<?= $this->render('layout/_head') ?>
<?= $this->render('layout/_header', ['title' => $title]) ?>


    <div class="container">

        <a href="/" class="btn btn-success">Go back</a>

        <br>

        <div>

            <?= $this->render('_formError', ['model' => $model]); ?>

            <form method="post" enctype="multipart/form-data" id="task-form">
                <div class="form-group">
                    <label>Author:</label>
                    <input name="author" value="<?= $model->author ?>" required class="form-control"/>
                </div>

                <div class="form-group">
                    <label>Email:</label>
                    <input type="email" value="<?= $model->email ?>" name="email" required class="form-control"/>
                </div>

                <div class="form-group">
                    <label>Text:</label>
                    <textarea name="text" required class="form-control"><?= $model->text ?></textarea>
                </div>

                <?php if ($isAdmin): ?>
                    <div class="form-group">
                        <label>Status:</label>
                        <select name="status" class="form-control">
                            <?php foreach (\app\models\Task::$statusNames as $value => $name): ?>
                                <option value="<?= $value ?>" <?= $value == $model->status ? 'selected' : null ?>>
                                    <?= $name ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="uploaded_file" class="form-control"/>
                    <?php if ($model->getTask()->getDataByName('image')): ?>
                        <img src="<?= $model->getTask()->getDataByName('image') ?>">
                    <?php endif ?>
                </div>

                <input type="submit" class="btn btn-success" value="Save"/>
                <input type="button" class="btn btn-info" onclick="taskPreview()" value="Preview"/>
            </form>
        </div>
    </div> <!-- /close container -->


    <!-- Modal -->
    <div class="modal fade" id="task-modal" tabindex="-1" role="dialog" aria-labelledby="task-modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="task-modal-label">Task preview</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('layout/_footer') ?>