<?php
/**
 * @var \app\interfaces\views\IView $this
 */
?>
<div class="masthead">
    <div class="container">
        <h1><?= $title ?></h1>
    </div>
</div>
