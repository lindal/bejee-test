<?= $this->render('layout/_head') ?>
<?= $this->render('layout/_header', ['title' => 'Task list']) ?>

    <div class="container">

        <div>
            <?php if (!$isAdmin): ?>
                <?= $this->render('_loginBlock') ?>
            <?php else: ?>
                <a href="/user/logout" class="btn btn-danger">
                    Logout
                </a>
            <?php endif ?>

            <a href="/task/create" class="btn btn-success">Create new task</a>
        </div>

        <br>
        <?= $this->render('_search', ['model' => $search]) ?>

        <hr>
        <div class="posts">

            <?php
            if ($items = $collection->getItems()) {
                $tasks = [];
                foreach ($items as $task) {
                    $tasks[] = $this->render('_task', [
                        'model' => $task,
                        'isAdmin' => $isAdmin
                    ]);
                }
                echo implode('<hr>', $tasks);
            } else {
                echo "<p>Nothing found</p>";
            }
            ?>

        </div>

        <?= $pagination->render(); ?>

    </div> <!-- /close container -->

<?= $this->render('layout/_footer') ?>