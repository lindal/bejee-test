<?php
/**
 * @var \app\models\Task $model
 */
?>
<div class="post">
    <span class="post-date">
        Date: <?= date('d.m.Y', strtotime($model->getDataByName('created_at'))) ?>
        <br>
        Author: <?= $this->encode($model->getDataByName('author')) ?>
        <br>
        Email: <?= $this->encode($model->getDataByName('email')) ?>
        <br>
        Status: <?= $model->getStatusName() ?>
    </span>

    <?php if ($model->getDataByName('image')): ?>
        <img src="<?= $model->getDataByName('image') ?>">
    <?php else: ?>
        <h3>Your picture</h3>
    <?php endif ?>

    <p>
        <?= nl2br($this->encode($model->getDataByName('text'))) ?>
    </p>

    <?php if ($isAdmin): ?>
        <div class="task-control">
            <a href="/task/update/<?= $model->getId() ?>" class="btn btn-info">
                Update
            </a>
        </div>

    <?php endif ?>
</div>