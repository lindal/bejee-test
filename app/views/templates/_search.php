<?php
/**
 * @var \app\models\forms\SearchTasks $model
 */
?>
<div>

    <?= $this->render('_formError', ['model' => $model]) ?>

    <form method="get" id="search-form" class="form-inline">
        <div class="form-group">
            <label>Order by:</label>
            <select name="order" class="form-control">
                <?php foreach ($model->allowedOrder as $order): ?>
                    <option value="<?= $order ?>" <?= $order == $model->order ? 'selected' : null ?>>
                        <?= $order ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label>Direction:</label>

            <select name="dir" class="form-control">
                <?php foreach ($model->allowedDir as $dir): ?>
                    <option value="<?= $dir ?>" <?= $dir == $model->dir ? 'selected' : null ?>>
                        <?= $dir ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <input class="btn btn-success" name="search" value="Order" type="submit">
        </div>

    </form>
</div>