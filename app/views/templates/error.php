<?php
/**
 * @var \app\interfaces\views\IView $this
 * @var Exception $exception
 */
echo $this->render('layout/_head');
?>
    <h1><?= $exception->getMessage() ?></h1>
    <p>
        <?php
        echo nl2br($exception->getTraceAsString());
        ?>
    </p>

<?= $this->render('layout/_footer') ?>