<?php
/**
 * @var $this \app\views\View
 * @var $widget \app\widgets\Pagination
 */
?>

<div class="pagination">
    <?php foreach ($widget->getItems() as $item): ?>
        <div class="pagination-item">
            <?php if ($item['current']) : ?>
                <span><?= $item['label'] ?></span>
            <?php else: ?>
                <a href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
            <?php endif ?>
        </div>
    <?php endforeach; ?>
</div>
