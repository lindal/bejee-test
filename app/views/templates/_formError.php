<?php
/**
 * @var \app\interfaces\models\IForm $model
 */
if ($model->getErrors()): ?>
    <div style="background-color: indianred">
        <h3>Errors:</h3>
        <ul>
            <?php foreach ($model->getErrors() as $error): ?>
                <li>
                    <?= $error['message'] ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>
