<?php

namespace app\views;

use app\interfaces\views\IView;

class View implements IView
{

    /**
     * Return base path for templates
     * @return string
     */
    public function getBaseTemplatePath(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * Return rendered template
     * @param string $template
     * @param array $params
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    public function render(string $template, array $params = []): string
    {
        $_obInitialLevel_ = ob_get_level();
        ob_start();
        ob_implicit_flush(false);
        extract($params, EXTR_OVERWRITE);
        try {
            require $this->getTemplatePath($template);
            return ob_get_clean();
        } catch (\Exception $e) {
            while (ob_get_level() > $_obInitialLevel_) {
                if (!@ob_end_clean()) {
                    ob_clean();
                }
            }
            throw $e;
        } catch (\Throwable $e) {
            while (ob_get_level() > $_obInitialLevel_) {
                if (!@ob_end_clean()) {
                    ob_clean();
                }
            }
            throw $e;
        }
    }

    private function getTemplatePath(string $template): string
    {
        return $this->getBaseTemplatePath() . $template . '.php';
    }

    /**
     * Remove all user tags
     * @param string $text
     * @return string
     */
    public function encode(string $text): string
    {
        return strip_tags($text);
    }
}