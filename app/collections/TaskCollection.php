<?php

namespace app\collections;


use app\interfaces\collections\ICollection;
use app\interfaces\models\IModel;
use app\interfaces\repositories\IRepository;
use app\models\Task;
use app\repositories\TaskRepository;

class TaskCollection implements ICollection
{

    private $_items = [];
    private $_size = 20;
    private $_page = 1;
    private $_totalCount;
    private $_filter = [];
    private $_order = [];
    private $_itemClass = Task::class;

    private $_repository;

    public function __construct(TaskRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->_items;
    }

    /**
     * @param IModel[] $items
     * @return ICollection
     */
    public function setItems(array $items): ICollection
    {
        foreach ($items as $item) {
            $this->addItem($item);
        }
        return $this;
    }

    /**
     * @param $item
     * @return ICollection
     */
    public function addItem(IModel $item): ICollection
    {
        $this->_items[$item->getId()] = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->_size;
    }

    /**
     * @param int $size
     * @return ICollection
     */
    public function setSize(int $size): ICollection
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->_page;
    }

    /**
     * @param int $page
     * @return ICollection
     */
    public function setCurrentPage(int $page): ICollection
    {
        $this->_page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return count($this->_items);
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        if (is_null($this->_totalCount)) {
            $this->load();
        }
        return $this->_totalCount;
    }

    /**
     * @param int $count
     * @return ICollection
     */
    public function setTotalCount(int $count): ICollection
    {
        $this->_totalCount = $count;
        return $this;
    }

    /**
     * Load collection from repository
     * @return ICollection
     */
    public function load(): ICollection
    {
        $repository = $this->_repository;
        $repository->setLimit($this->_size);
        $repository->setOffset(($this->_page - 1) * $this->_size);
        foreach ($this->_filter as $filter) {
            $repository->addFilter($filter[0], $filter[1], $filter[2]);
        }

        foreach ($this->_order as $field => $dir) {
            $repository->addOrder($field, $dir);
        }

        $data = $repository->load();
        foreach ($data as $row) {
            $this->addItem($this->createItem($row));
        }

        $this->_totalCount = $repository->getCount();
        return $this;
    }

    /**
     * @param array $data
     * @return IModel
     */
    private function createItem(array $data): IModel
    {
        $className = $this->_itemClass;
        $item = new $className($this->_repository);
        $item->setId($data['id']);
        unset($data['id']);
        $item->setData($data);
        return $item;
    }

    /**
     * @param IRepository $repository
     * @return ICollection
     */
    public function setRepository(IRepository $repository): ICollection
    {
        $this->_repository = $repository;
        return $this;
    }

    /**
     * @return IRepository
     */
    public function getRepository(): IRepository
    {
        return $this->_repository;
    }

    /**
     * @param string $field
     * @param string $dir
     * @return ICollection
     */
    public function addOrder(string $field, string $dir = 'ASC'): ICollection
    {
        $this->_order[$field] = $dir;
        return $this;
    }

    /**
     * @return ICollection
     */
    public function clearOrder(): ICollection
    {
        $this->_order = [];
        return $this;
    }

    /**
     * @param string $name Attribute name
     * @param string $value
     * @param string $type
     * @return ICollection
     */
    public function addFilter(string $name, string $value, string $type = 'AND'): ICollection
    {
        $this->_filter[] = [$name, $value, $type];
        return $this;
    }

    /**
     * @return ICollection
     */
    public function clearFilter(): ICollection
    {
        $this->_filter = [];
        return $this;
    }
}