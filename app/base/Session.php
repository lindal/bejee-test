<?php

namespace app\base;


class Session
{

    /**
     * @param string $name
     * @return null
     */
    public function get(string $name)
    {
        return $_SESSION[$name] ?? null;
    }

    /**
     * @param string $name
     * @param $value
     * @return Session
     */
    public function set(string $name, $value): Session
    {
        $_SESSION[$name] = $value;
        return $this;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return isset($_SESSION[$name]);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->get('is_admin') == true;
    }

    /**
     * @param bool $isAdmin
     * @return Session
     */
    public function setIsAdmin(bool $isAdmin): Session
    {
        $this->set('is_admin', $isAdmin);
        return $this;
    }

}