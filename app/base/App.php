<?php

namespace app\base;


use app\controllers\ErrorController;
use app\views\View;
use DI\ContainerBuilder;
use lindal\webhelper\Request;
use lindal\webhelper\Response;
use lindal\webhelper\routing\Router;
use lindal\webhelper\routing\Rule;

class App
{

    public $container;

    public $request;

    public $response;

    public $router;

    private static $_config = [];

    private static $_instance;

    public function __construct()
    {
        $builder = new ContainerBuilder();
        if (isset(self::$_config['definitions'])) {
            $builder->addDefinitions(self::$_config['definitions']);
        }

        $this->response = Response::getInstance();
        $this->request = Request::getInstance();
        $this->router = new Router(
            $this->request,
            $this->response
        );

        $this->container = $builder->build();

        if (isset(self::$_config['routing'])) {
            foreach (self::$_config['routing'] as $ruleData) {
                $this->router
                    ->addRule(new Rule(
                        $ruleData['className'],
                        $ruleData['handler'],
                        $ruleData['pattern'],
                        $ruleData['method'] ?? 'GET'
                    ));
            }
        }
    }

    /**
     * @return App
     */
    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new App();
        }
        return self::$_instance;
    }

    /**
     * Run application
     * @param array $config
     */
    public static function run(array $config)
    {
        session_start();
        self::$_config = $config;
        self::$_instance = new App();
        try {
            self::$_instance->router->execute();
        } catch (\Exception $e) {
            $controller = self::$_instance
                ->container->make(ErrorController::class);
            $controller->indexAction(
                self::$_instance->request,
                self::$_instance->response,
                $e
            );
        }
    }

    public static function getBaseDir()
    {
        return __DIR__ . '/../../';
    }

}