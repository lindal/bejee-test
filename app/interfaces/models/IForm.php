<?php

namespace app\interfaces\models;


interface IForm
{

    /**
     * Validate data
     * @return bool
     */
    public function validate(): bool;

    /**
     * Add error to errors list
     * @param string $attribute
     * @param string $message
     * @return IForm
     */
    public function addError(string $attribute, string $message): IForm;

    /**
     * Return errors list
     * @return array
     */
    public function getErrors(): array;
}