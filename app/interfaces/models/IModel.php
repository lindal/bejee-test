<?php

namespace app\interfaces\models;


use app\interfaces\repositories\IRepository;

interface IModel
{

    /**
     * @param string $name
     * @return mixed
     */
    public function getDataByName(string $name);

    /**
     * @param string $name
     * @param mixed $value
     * @return IModel
     */
    public function setDataByName(string $name, $value): IModel;

    /**
     * Set data to model
     * @param array $data
     * @return IModel
     */
    public function setData(array $data): IModel;

    /**
     * Return all data
     * @return array
     */
    public function getData(): array;

    /**
     * Return model ID
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     * @return IModel
     */
    public function setId($id): IModel;

    /**
     * @param $id
     * @return IModel
     */
    public function load($id): IModel;

    /**
     * Save model into repository
     * @return null
     */
    public function save();

    /**
     * @param IRepository $repository
     * @return IModel
     */
    public function setRepository(IRepository $repository): IModel;

    /**
     * Return model`s repository or null if not set
     * @return IRepository|null
     */
    public function getRepository();

}