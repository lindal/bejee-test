<?php

namespace app\interfaces\validators;

use app\interfaces\models\IForm;

interface IValidator
{

    /**
     * @param IForm $model
     * @param string $attribute
     * @param array $params
     * @return bool
     */
    public function run(IForm $model, string $attribute, array $params = []): bool;

}