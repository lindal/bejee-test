<?php

namespace app\interfaces\repositories;


use app\interfaces\models\IModel;

interface IRepository
{
    /**
     * Load model by ID
     * Return NULL if nothing found
     * @param int $id
     * @return array|null
     */
    public function loadById(int $id);

    /**
     * Add filter to query
     * @param string $field
     * @param string $value
     * @param string $type
     * @return IRepository
     */
    public function addFilter(string $field, string $value, string $type = 'and'): IRepository;

    /**
     * Clearc filter data
     * @return IRepository
     */
    public function clearFilter(): IRepository;

    /**
     * @param string $field
     * @param string $dir
     * @return IRepository
     */
    public function addOrder(string $field, string $dir = 'ASC'): IRepository;

    /**
     * Clear order data
     * @return IRepository
     */
    public function clearOrder(): IRepository;

    /**
     * Get limit
     * @return int
     */
    public function getLimit(): int;

    /**
     * Set limit
     * @param int $limit
     * @return IRepository
     */
    public function setLimit(int $limit): IRepository;

    /**
     * Return offset
     * @return int
     */
    public function getOffset(): int;

    /**
     * Set current page
     * @param int $offset
     * @return IRepository
     */
    public function setOffset(int $offset): IRepository;

    /**
     * Return result
     * @return array
     */
    public function load(): array;

    /**
     * @param array $data
     * @return int Inserted id
     */
    public function insert(array $data): int;

    /**
     * @param int $id
     * @param array $data
     * @return bool Is updated
     */
    public function update(int $id, array $data): bool;

    /**
     * Delete row by ID
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * Return total count
     * @return int
     */
    public function getCount(): int;
}