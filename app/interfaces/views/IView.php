<?php

namespace app\interfaces\views;

interface IView
{

    /**
     * Return base path for templates
     * @return string
     */
    public function getBaseTemplatePath(): string;

    /**
     * Return rendered template
     * @param string $template
     * @param array $params
     * @return string
     */
    public function render(string $template, array $params = []): string;

    /**
     * Remove all user tags
     * @param string $text
     * @return string
     */
    public function encode(string $text): string;

}