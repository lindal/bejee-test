<?php

namespace app\interfaces\collections;

use app\interfaces\models\IModel;
use app\interfaces\repositories\IRepository;

interface ICollection
{

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @param IModel[] $items
     * @return ICollection
     */
    public function setItems(array $items): ICollection;

    /**
     * @param $item
     * @return ICollection
     */
    public function addItem(IModel $item): ICollection;

    /**
     * @return int
     */
    public function getSize(): int;

    /**
     * @param int $size
     * @return ICollection
     */
    public function setSize(int $size): ICollection;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @param int $page
     * @return ICollection
     */
    public function setCurrentPage(int $page): ICollection;

    /**
     * @return int
     */
    public function getCount(): int;

    /**
     * @return int
     */
    public function getTotalCount(): int;

    /**
     * @param int $count
     * @return ICollection
     */
    public function setTotalCount(int $count): ICollection;

    /**
     * Load collection from repository
     * @return ICollection
     */
    public function load(): ICollection;

    /**
     * @param IRepository $repository
     * @return ICollection
     */
    public function setRepository(IRepository $repository): ICollection;

    /**
     * @return IRepository
     */
    public function getRepository(): IRepository;

    /**
     * @param string $field
     * @param string $dir
     * @return ICollection
     */
    public function addOrder(string $field, string $dir = 'ASC'): ICollection;

    /**
     * @return ICollection
     */
    public function clearOrder(): ICollection;

    /**
     * @param string $name
     * @param string $value
     * @param string $type
     * @return ICollection
     */
    public function addFilter(string $name, string $value, string $type = 'AND'): ICollection;

    /**
     * @return ICollection
     */
    public function clearFilter(): ICollection;

}